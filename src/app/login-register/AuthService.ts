import { Injectable } from '@angular/core';
import decode from 'jwt-decode';
import { tokenNotExpired } from 'angular2-jwt';


@Injectable()
// @ts-ignore
export class AuthService {
  public getToken(): string {
    return localStorage.getItem('auth');
  }
  public isAuthenticated(): boolean {
    // get the token
    const token = this.getToken();
    // return a boolean reflecting
    // whether or not the token is expired
    return tokenNotExpired(null, token);
  }
}
