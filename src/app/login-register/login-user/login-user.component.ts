import { Component, OnInit } from '@angular/core';
// tslint:disable-next-line:import-spacing
import { Observable, of, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { HttpClient, HttpHandler, HttpHeaders, HttpRequest, HttpEvent, HttpResponse } from '@angular/common/http';
import { UserService } from '../../user-list/models/user.service';

@Component({
  selector: 'app-login-user',
  templateUrl: './login-user.component.html',
  styleUrls: ['./login-user.component.css']
})
export class LoginUserComponent implements OnInit {

  constructor(private http: HttpClient, private router: Router, private service: UserService) { }
  UserLogin = {
    username: '',
    password: ''
  };
  helper = new JwtHelperService();
  isValid = false;
  errorMessage = '';


  ngOnInit(): void {
    this.service.isloged = false;
  }

  // tslint:disable-next-line:typedef
  // @ts-ignore
  // tslint:disable-next-line:typedef
  onClickLogin(username, password) {
    this.UserLogin.username = username;
    this.UserLogin.password = password;
    this.http.post<HttpResponse<any>>('http://localhost:9000/login', this.UserLogin, { observe: 'response' })
      .pipe(map(response => {
        localStorage.setItem('auth', response.headers.get('authorization'));
        this.isValid = true;
        this.service.isloged = true;
        this.service.decodedToken = this.service.getDecodedAccessToken(localStorage.getItem("auth"))
        if (this.isValid) {
          this.router.navigate(['/adminpage']);
        } else {
          return console.log('Not valid');
        }
      }))
      .subscribe(
        data => { },
        err => {
          this.errorMessage = 'Invalid credentials';
          this.isValid = false;
        }
      );
  }
}


