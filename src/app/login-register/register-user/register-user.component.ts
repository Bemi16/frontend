import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { catchError, map } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';

@Component({
  selector: 'app-register-user',
  templateUrl: './register-user.component.html',
  styleUrls: ['./register-user.component.css']
})
export class RegisterUserComponent implements OnInit {

  constructor(private http: HttpClient, private router: Router) { }
  jwt = '';
  UserRegister = {
    firstName: '',
    lastName: '',
    username: '',
    password: '',
    email: '',
    userRole: 'client'
  };
  helper = new JwtHelperService();
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: ''
    })
  };
  isValid = false;
  errorMessage = '';
  ngOnInit(): void {
  }

  // tslint:disable-next-line:typedef
  onClickRegister(firstName, lastName, username, password, email) {
    this.UserRegister.firstName = firstName;
    this.UserRegister.lastName = lastName;
    this.UserRegister.username = username;
    this.UserRegister.password = password;
    this.UserRegister.email = email;
    console.log(this.UserRegister);
    this.http.post('http://localhost:9000/view/users', this.UserRegister).subscribe({
      next: data => {
        console.log(data);
        this.router.navigate(['/signin']);
        this.isValid = true;
      },
      error: error => {
        this.errorMessage = 'User already exists';
        this.isValid = false;
      }
    });
  }
}

