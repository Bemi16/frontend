import { Component, OnInit } from '@angular/core';
import {Author} from '../models/author';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthorService} from '../models/author.service';

@Component({
  selector: 'app-author-update',
  templateUrl: './author-update.component.html',
  styleUrls: ['./author-update.component.css']
})
export class AuthorUpdateComponent implements OnInit {
  id: number;
  author: Author;

  constructor(private route: ActivatedRoute, private router: Router,
              private authorService: AuthorService) { }

  // tslint:disable-next-line:typedef
  ngOnInit() {
    this.author = new Author();
    this.id = this.route.snapshot.params['id'];

    this.authorService.getAuthor(this.id)
      .subscribe(data => {
        console.log(data)
        this.author = data;
      }, error => console.log(error));
  }

  // tslint:disable-next-line:typedef
  updateAuthor() {
    this.authorService.updateAuthor(this.id, this.author)
      .subscribe(data => console.log(data), error => console.log(error));
    this.author = new Author();
    this.gotoList();
  }

  // tslint:disable-next-line:typedef
  onSubmit() {
    this.updateAuthor();
  }

  // tslint:disable-next-line:typedef
  gotoList() {
    this.router.navigate(['/authorlist']);
  }
}
