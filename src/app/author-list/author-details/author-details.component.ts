import { Component, OnInit } from '@angular/core';
import {Author} from "../models/author";
import {ActivatedRoute, Router} from "@angular/router";
import {AuthorService} from "../models/author.service";

@Component({
  selector: 'app-author-books',
  templateUrl: './author-details.component.html',
  styleUrls: ['./author-details.component.css']
})
export class AuthorDetailsComponent implements OnInit {

  id: number;
  author: Author;

  constructor(private route: ActivatedRoute, private router: Router,
              private authorService: AuthorService) { }

  // tslint:disable-next-line:typedef
  ngOnInit() {
    this.author = new Author();

    this.id = this.route.snapshot.params['id'];

    this.authorService.getAuthor(this.id)
      .subscribe(data => {
        console.log(data)
        this.author = data;
      }, error => console.log(error));
  }

  // tslint:disable-next-line:typedef
  list(){
    this.router.navigate(['authorlist']);
  }

}
