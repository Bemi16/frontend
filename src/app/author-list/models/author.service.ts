import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Author } from '../models/author';

@Injectable({
  providedIn: 'root'
})
export class AuthorService {

  private baseUrl = 'http://localhost:9000/view/authors';

  constructor(private http: HttpClient) { }

  getAuthorList(): Observable<any> {
    return this.http.get<Author[]>(this.baseUrl + "/page/0");
  }

  getAuthor(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  createAuthor(employee: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}`, employee);
  }

  updateAuthor(id: number, value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/${id}`, value);
  }

  deleteAuthor(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`, { responseType: 'text' });
  }


}
