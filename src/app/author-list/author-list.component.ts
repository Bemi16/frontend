import { Component, OnInit } from '@angular/core';
<<<<<<< HEAD
import { Observable } from 'rxjs';
import { Author } from './models/author';
import { Router } from '@angular/router';
import { AuthorService } from './models/author.service';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { of } from 'rxjs';
=======
import {Observable} from 'rxjs';
import {Author} from './models/author';
import {Router} from '@angular/router';
import {AuthorService} from './models/author.service';

>>>>>>> 873170fd8478ba719d668b10e8a3171e461bbd52
@Component({
  selector: 'app-author-list',
  templateUrl: './author-list.component.html',
  styleUrls: ['./author-list.component.css']
})
export class AuthorListComponent implements OnInit {

<<<<<<< HEAD
  authors: [];


  author = {
    name: "",
    biography: ""
  }

  list;

  constructor(private authorService: AuthorService,
    private router: Router, private http: HttpClient) { }
=======
  authors: Observable<Author[]>;

  constructor(private authorService: AuthorService,
              private router: Router) {}
>>>>>>> 873170fd8478ba719d668b10e8a3171e461bbd52

  // tslint:disable-next-line:typedef
  ngOnInit() {
    this.reloadData();
  }

  // tslint:disable-next-line:typedef
  reloadData() {
    this.authorService.getAuthorList().subscribe(data => {
      this.authors = data.content
    })
  }


  addAuthor(name, biography) {
    this.author.name = name;
    this.author.biography = biography

    this.http.post('http://localhost:9000/view/authors', this.author).subscribe(
      data => console.log(data),
      err => console.log(err)
    );

    location.reload();
  }

  // tslint:disable-next-line:typedef
  deleteAuthor(name) {
    this.http.delete('http://localhost:9000/view/authors/' + name).subscribe();
    location.reload();
  }

  // tslint:disable-next-line:typedef
  authorDetails(id: number) {
    this.router.navigate(['authordetails', id]);
  }

  // tslint:disable-next-line:typedef
  updateAuthor(presentName, newName, biography) {
    this.author.name = newName;
    this.author.biography = biography
    this.http.put('http://localhost:9000/view/authors/' + presentName, this.author).subscribe();

    setTimeout(function () { location.reload(); }, 500);

  }


}
