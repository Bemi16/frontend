import { Component, OnInit } from '@angular/core';
import { UserService } from '../user-list/models/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(private service: UserService, private router: Router) { }


  isLoged;
  ngOnInit(): void {
  }


  home() {
    if (this.service.isloged === false) {
      this.isLoged = false;
    }
    else {
      this.service.decodedToken = this.service.getDecodedAccessToken(localStorage.getItem("auth"))
      this.router.navigate(['/adminpage']);
    }
  }


}
