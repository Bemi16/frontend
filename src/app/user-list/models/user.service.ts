import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import * as jwt_decode from "jwt-decode";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  loged;
  user;
  username;
  tokenDecoder;
  decodedToken;
  isloged;;
  getDecodedAccessToken(token) {
    try {
      return jwt_decode(token);
    }
    catch (Error) {
      return null;
    }
  }

  decodedToken;

  baseUrl = 'http://localhost:9000/view/users';
  jwtTokken = '';
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: this.jwtTokken
    })
  };
  constructor(private http: HttpClient, private router: Router) { }


  getUser(username: string): Observable<any> {
    return this.http.get(`${this.baseUrl}/${username}`);
  }

  // tslint:disable-next-line:ban-types typedef
  async updateUser() {
    console.log(this.username);
    await this.http.put(this.baseUrl + '/' + this.username, this.user).subscribe(
      data => {
        console.log(data)
        this.router.navigateByUrl('/userlist');
      },
      err => console.log(err)
    );
  }
}
