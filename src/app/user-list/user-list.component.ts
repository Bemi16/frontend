import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { UserService } from './models/user.service';
import { User } from './models/user';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  constructor(private userService: UserService, private router: Router, private http: HttpClient) {
  }

  usersData;
  users;
  // tslint:disable-next-line:typedef
  ngOnInit() {
    this.reloadData();
    var refresh = localStorage.getItem('refresh');
    console.log(refresh);
    if (refresh === null) {
      window.location.reload();
      localStorage.setItem('refresh', "1");
    }
  }
  // tslint:disable-next-line:typedef
  reloadData() {
    return this.http.get(this.userService.baseUrl + "/page/0").subscribe(
      data => {
        this.usersData = data;
        this.users = this.usersData.content
      },
      err => console.log(err)
    );
  }

  // tslint:disable-next-line:typedef
  deleteUser(username) {
    return this.http.delete(`http://localhost:9000/view/users/` + username).subscribe(
      data =>
        console.log("da"),
      error => this.reloadData());
  }


  // tslint:disable-next-line:typedef
  updateUser(user) {
    // @ts-ignore
    this.userService.username = user.username;
    this.userService.user = user;
    this.router.navigate(['updateuser']);

  }
  // tslint:disable-next-line:typedef
  userDetails(username: string) {
    // @ts-ignore
    this.router.navigate(['userdetails'], username);
  }



}
