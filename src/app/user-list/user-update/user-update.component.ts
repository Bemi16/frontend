import { Component, OnInit } from '@angular/core';
import { User } from '../models/user';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../models/user.service';

@Component({
  selector: 'app-user-update',
  templateUrl: './user-update.component.html',
  styleUrls: ['./user-update.component.css']
})
export class UserUpdateComponent implements OnInit {

  UpdateForm = {
    firstName: '',
    lastName: '',
    username: '',
    password: '',
    email: '',
    userRole: 'client'
  };
  constructor(private route: ActivatedRoute, private router: Router,
    private userService: UserService) { }

  ngOnInit(): void {

  }
  // tslint:disable-next-line:typedef
  onSubmit() {
    this.userService.user = this.UpdateForm;
    this.userService.updateUser();
  }

}
