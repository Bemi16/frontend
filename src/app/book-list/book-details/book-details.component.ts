import { Component, OnInit } from '@angular/core';
import {User} from '../../user-list/models/user';
import {ActivatedRoute, Router} from '@angular/router';
import {Book} from '../models/book';
import {BookService} from '../models/book.service';

@Component({
  selector: 'app-book-details',
  templateUrl: './book-details.component.html',
  styleUrls: ['./book-details.component.css']
})
export class BookDetailsComponent implements OnInit {

  id: number;
  book: Book;

  constructor(private route: ActivatedRoute, private router: Router,
              private bookService: BookService) { }

  // tslint:disable-next-line:typedef
  ngOnInit() {
    this.book = new Book();

    this.id = this.route.snapshot.params['id'];

    this.bookService.getBook(this.id)
      .subscribe(data => {
        console.log(data)
        this.book = data;
      }, error => console.log(error));
  }

  // tslint:disable-next-line:typedef
  list(){
    this.router.navigate(['userlist']);
  }


}
