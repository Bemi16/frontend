import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BookService } from '../models/book.service';
import { UserService } from '../../user-list/models/user.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'myBooks',
  templateUrl: './myBooks.component.html',
  styleUrls: ['./myBooks.component.css'],
})
export class BookShopComponent implements OnInit {

  constructor(private bookService: BookService, private route: ActivatedRoute,
    private router: Router, private http: HttpClient, private service: UserService) {

  }

  list;
  books: any;

  name;

  book = {
    title: "",
    publisher: "",
    category: "",
    stock: ""
  };

  // tslint:disable-next-line:typedef
  ngOnInit() {
    this.name = localStorage.getItem("username")
    console.log(this.name);
    this.reloadData();
  }


  // tslint:disable-next-line:typedef
  reloadData() {
    this.list = this.http.get("http://localhost:9000/view/users/" + this.name + "/books").subscribe(
      data => {
        this.books = data;
      },
      err => {
        console.log(err)
      }
    )
  }

  // tslint:disable-next-line:typedef
  deleteBook(id: number) {
    this.bookService.deleteBook(id)
      .subscribe(
        data => {
          this.reloadData();
        },
        error => console.log(error));
  }

  // tslint:disable-next-line:typedef
  bookDetails(id: number) {
    this.router.navigate(['details', id]);
  }

  // tslint:disable-next-line:typedef
  updateBook(id: number) {
    this.router.navigate(['update', id]);
  }


  returnBook(title) {
    this.http.post("http://localhost:9000/view/users/" + this.name + "/returnbook/" + title, null).subscribe();
  }

  addBook(title, publisher, category, stock) {
    this.book.title = title;
    this.book.category = category;
    this.book.stock = stock;


    this.http.post('http://localhost:9000/view/authors/' + publisher + '/books', this.book).subscribe(
      data => console.log(data),
      err => console.log(err)
    );
  }
  open(content) {
  }
}
