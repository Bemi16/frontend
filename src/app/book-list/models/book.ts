export class Book {
  id: number;
  title: string;
  author: string;
  publisher: string;
  category: string;
  active: boolean;
}
