import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Book } from '../models/book';
import { BookService } from '../models/book.service';

@Component({
  selector: 'app-update-book',
  templateUrl: './update-book.component.html',
  styleUrls: ['./update-book.component.css']
})
export class UpdateBookComponent implements OnInit {

  id: number;
  book: Book;
  constructor(private route: ActivatedRoute, private router: Router,
    private bookService: BookService) { }

  ngOnInit(): void {
    this.book = new Book();
    this.id = this.route.snapshot.params.id;

    this.bookService.getBook(this.id);
  }

  // tslint:disable-next-line:typedef
  updateBook() {
    this.bookService.updateBook(this.id, this.book);
    this.book = new Book();
    this.gotoList();
  }

  // tslint:disable-next-line:typedef
  onSubmit() {
    this.updateBook();
  }

  // tslint:disable-next-line:typedef
  gotoList() {
    this.router.navigate(['/booklist']);
  }


}
