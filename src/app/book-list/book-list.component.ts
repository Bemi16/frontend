import { Component, OnInit } from '@angular/core';
import { Book } from './models/book';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { BookService } from './models/book.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css']
})
export class BookListComponent implements OnInit {

  constructor(private bookService: BookService,
    private router: Router, private http: HttpClient) { }

  list: any;
  books = [];

  authList: any;
  authors = [];

  selectAuth;


  newBook;
  book = {
    title: "",
    publisher: "",
    category: "",
    stock: ""
  };

  // tslint:disable-next-line:typedef
  ngOnInit() {
    this.reloadData();
    this.getAuthros();
  }

  // tslint:disable-next-line:typedef
  reloadData() {
    this.bookService.getBookList().subscribe(data => {
      this.list = data,
        this.books = this.list.content
      err =>
        console.log(err)
    })
  }


  deleteBook(title) {
    this.http.delete('http://localhost:9000/view/authors/books/' + title).subscribe();
    setTimeout(function () { location.reload(); }, 500);
  }
  // tslint:disable-next-line:typedef
  bookDetails(id: number) {
    this.router.navigate(['details', id]);
  }


  // tslint:disable-next-line:typedef
  updateBook(presentTitle, title, category, stock, publisher) {
    console.log(title);
    console.log(this.selectAuth)
    this.book.title = title;
    this.book.category = category;
    this.book.stock = stock;
    this.book.publisher = publisher;

    this.newBook = this.book;

    console.log(this.newBook)
    this.http.put("http://localhost:9000/view/authors/" + this.selectAuth + "/books/" + presentTitle, this.newBook).subscribe(
      data => { console.log(data) },
      err => {
        console.log(err)
      }
    )
    setTimeout(function () { location.reload(); }, 800);

  }


  getAuthros() {
    this.http.get('http://localhost:9000/view/authors/page/0').subscribe(
      data => {
        this.authList = data;
        this.authors = this.authList.content;
        console.log(this.authors)
      },
      err => console.log(err)
    );
  }



  addBook(title, publisher, category, stock) {
    console.log(this.selectAuth)

    this.book.title = title;
    this.book.category = category;
    this.book.stock = stock;
    this.book.publisher = publisher;
    this.newBook = this.book;

    this.http.post('http://localhost:9000/view/authors/' + this.selectAuth + '/books', this.newBook).subscribe();
    setTimeout(function () { location.reload(); }, 500);
  }
}


