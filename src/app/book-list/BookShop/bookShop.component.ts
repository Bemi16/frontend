import { Component, OnInit } from '@angular/core';
import { Book } from '../models/book';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { BookService } from '../models/book.service';
import { UserService } from '../../user-list/models/user.service';

import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'bookShop',
  templateUrl: './bookShop.component.html',
  styleUrls: ['./bookShop.component.css']
})
export class MybooksComponent implements OnInit {

  constructor(private bookService: BookService,
    private router: Router, private http: HttpClient, private service: UserService) { }

  list: any;
  books: [];

  username;






  book = {
    title: "",
    publisher: "",
    category: "",
    stock: ""
  };

  author;

  // tslint:disable-next-line:typedef
  ngOnInit() {
    this.reloadData();

  }

  // tslint:disable-next-line:typedef
  reloadData() {
    this.bookService.getBookList().subscribe(data => {
      this.list = data;
      this.books = this.list.content
    },
      err => {
        console.log(err)
      })
  }

  // tslint:disable-next-line:typedef
  deleteBook(id: number) {
    this.bookService.deleteBook(id)
      .subscribe(
        data => {
          this.reloadData();
        },
        error => console.log(error));
  }

  // tslint:disable-next-line:typedef
  bookDetails(id: number) {
    this.router.navigate(['details', id]);
  }

  rentBook(title) {
    this.username = localStorage.getItem("username");
    console.log(this.username)
    console.log(title);
    this.http.post("http://localhost:9000/view/users/" + this.username + "/loanbook/" + title, null).subscribe();
    setTimeout(function () { alert("Hello"); }, 3000);
  }

  // tslint:disable-next-line:typedef
  updateBook(id: number) {
    this.router.navigate(['update', id]);
  }

  addBook(title, publisher, category, stock) {
    this.book.title = title;
    this.book.category = category;
    this.book.stock = stock;


    this.http.post('http://localhost:9000/view/authors/' + publisher + '/books', this.book).subscribe(
      data => console.log(data),
      err => console.log(err)
    );
  }
  getAuthor(title) {

  }
}
