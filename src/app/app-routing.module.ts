import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterUserComponent } from './login-register/register-user/register-user.component';
import { AdminPageComponent } from './admin-page/admin-page.component';
import { LoginUserComponent } from './login-register/login-user/login-user.component';
import { BookListComponent } from './book-list/book-list.component';
import { AddBooksComponent } from './book-list/add-books/add-books.component';
import { UserListComponent } from './user-list/user-list.component';
import { UserDetailsComponent } from './user-list/user-details/user-details.component';
import { UserUpdateComponent } from './user-list/user-update/user-update.component';
import { AuthorListComponent } from './author-list/author-list.component';
import { AuthorUpdateComponent } from './author-list/update-author/author-update.component';
import { AuthorDetailsComponent } from './author-list/author-details/author-details.component';
import { UpdateBookComponent } from '../app/book-list/update-book/update-book.component'
import { BookShopComponent } from './book-list/MyBook/myBooks.component'
import { MybooksComponent } from './book-list/BookShop/bookShop.component'

const routes: Routes = [
  { path: 'adminpage', component: AdminPageComponent },
  { path: 'register', component: RegisterUserComponent },
  { path: 'signin', component: LoginUserComponent },
  { path: 'booklist', component: BookListComponent },
  { path: 'addbook', component: AddBooksComponent },
  { path: 'userlist', component: UserListComponent },
  { path: 'userdetails', component: UserDetailsComponent },
  { path: 'updateuser', component: UserUpdateComponent },
  { path: 'authorlist', component: AuthorListComponent },
  { path: 'updateauthor', component: AuthorUpdateComponent },
  { path: 'authordetails', component: AuthorDetailsComponent },
  { path: 'updateBook', component: UpdateBookComponent },
  { path: 'myBooks', component: BookShopComponent },
  { path: 'bookStore', component: MybooksComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
