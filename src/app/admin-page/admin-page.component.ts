import { Component, OnInit } from '@angular/core';
import { UserService } from '../user-list/models/user.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';


@Component({
  selector: 'app-add-book',
  templateUrl: './admin-page.component.html',
  styleUrls: ['./admin-page.component.css']
})
export class AdminPageComponent implements OnInit {

  constructor(private service: UserService, private http: HttpClient, private router: Router) { }
  clientRoles = this.service.decodedToken;
  isAdmin = false;;
  authorities = this.clientRoles.authorities;

  name = this.clientRoles.sub;

  ngOnInit(): void {
    this.service.isloged = true;
    for (let i in this.authorities) {
      console.log(this.authorities[i].authority)
      if (this.authorities[i].authority == "ROLE_ADMIN") {
        this.isAdmin = true;
      }
    }
  }

  myBooks() {
    localStorage.setItem("username", this.name);
    this.router.navigate(['myBooks']);
  }
  // tslint:disable-next-line:typedef
  getAllUsers() {
  }

}
