import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RegisterUserComponent } from './login-register/register-user/register-user.component';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatTabsModule } from '@angular/material/tabs';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatIconModule } from '@angular/material/icon';
import { NavbarComponent } from './navbar/navbar.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { AdminPageComponent } from './admin-page/admin-page.component';
import { LoginUserComponent } from './login-register/login-user/login-user.component';
import { BookListComponent } from './book-list/book-list.component';
import { AddBooksComponent } from './book-list/add-books/add-books.component';
import { UserListComponent } from './user-list/user-list.component';
import { UserDetailsComponent } from './user-list/user-details/user-details.component';
import { UserUpdateComponent } from './user-list/user-update/user-update.component';
import { AuthorListComponent } from './author-list/author-list.component';
import { AuthorUpdateComponent } from './author-list/update-author/author-update.component';
import { AuthorDetailsComponent } from './author-list/author-details/author-details.component';
import { UpdateBookComponent } from './book-list/update-book/update-book.component';
import { BookDetailsComponent } from './book-list/book-details/book-details.component';
import { TokenInterceptor } from './user-list/models/TokenInterceptor';
import { BookShopComponent } from './book-list/MyBook/myBooks.component';
import { MybooksComponent } from './book-list/BookShop/bookShop.component';





// @ts-ignore
@NgModule({
  declarations: [
    AppComponent,
    RegisterUserComponent,
    NavbarComponent,
    AdminPageComponent,
    LoginUserComponent,
    BookListComponent,
    AddBooksComponent,
    UserListComponent,
    UserDetailsComponent,
    UserUpdateComponent,
    AuthorListComponent,
    AuthorUpdateComponent,
    AuthorDetailsComponent,
    UpdateBookComponent,
    BookDetailsComponent,
    MybooksComponent,
    BookShopComponent,


  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    MatInputModule,
    MatCardModule,
    MatTabsModule,
    MatFormFieldModule,
    MatButtonModule,
    MatCheckboxModule,
    MatIconModule,
    MatToolbarModule,
    AppRoutingModule,
    HttpClientModule,


  ],
  exports: [

  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptor,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
